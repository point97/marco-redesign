"""
Django settings for marco project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '3muq#tos+krrh0*-pgw2yypo1v#^a0ad_3-_hn&w@hlhi%w^hg'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    
    # 3rd party
    'flatblocks',
    
    # madrona
    'features',
    'manipulators',
    'openid_auth',
    'user_profile',
    'forms',
    'scenarios',
    
    # marine planner
    'mp_profile',
    'data_manager',
    'visualize',
    'drawing',
    'clipping',
    'explore',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'marco.urls'

WSGI_APPLICATION = 'marco.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'marco17',
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}

SESSION_ENGINE = "django.contrib.sessions.backends.cache"

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(os.path.dirname(__file__), '..', 'static'),
)


# Configuration Management
# Simple .ini-based configuration override file. 

CONFIG_FILE = os.path.normpath(os.path.join(BASE_DIR, 'config.ini'))

import warnings
from ConfigParser import RawConfigParser

class RawConfigParserWithDefaults(RawConfigParser):
    """RawConfigParser modified to have return a default value if something
    doesn't exist rather than throwing a NoSectionError.
    """ 
    def get(self, section, name, default=None):
        if self.has_option(section, name):
            return self.get(section, name)
        else:
            if default is None or self.has_section(section):
                warnings.warn('Configuration missing: %s.%s\n' % (section, name))
            return default
    
    def getboolean(self, section, option, default=None):
        result = self.get(section, option, default)
        if type(result) is bool: 
            return result
        elif result.lower() in ('0', 'no', 'false'):
            return False
        else:
            return True


config = RawConfigParserWithDefaults()
config.read(CONFIG_FILE)

# Everything below here is set to defaults or overridden by the config file 

GROUP_REQUEST_EMAIL = config.get('auth', 'GROUP_REQUEST_EMAIL')

GEOMETRY_DB_SRID = config.get('OPTIONS', 'GEOMETRY_DB_SRID', 99996)
GEOMETRY_CLIENT_SRID = config.get('OPTIONS', 'GEOMETRY_CLIENT_SRID', 3857)
GEOJSON_SRID = config.get('OPTIONS', 'GEOJSON_SRID', 3857)
GEOJSON_DOWNLOAD = config.get('OPTIONS', 'GEOJSON_DOWNLOAD', False)

# Options to enable the object sharing features of madrona
ENABLE_SHARABLE_OBJECTS = config.getboolean('SHARABLE_OBJECTS',
                                            'ENABLE_SHARABLE_OBJECTS', False)
SHARING_TO_PUBLIC_GROUPS = config.getboolean('SHARABLE_OBJECTS', 
                                             'SHARING_TO_PUBLIC_GROUPS', False)
