from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'marco.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('openid_auth.urls')),
    
    url(r'^marco_profile/', include('mp_profile.urls')),
#     (r'^sdc/', include('scenarios.urls')),
    url(r'^drawing/', include('drawing.urls')),
    url(r'^data_manager/', include('data_manager.urls')),
#     (r'^learn/', include('learn.urls')),
    url(r'^scenario/', include('scenarios.urls')),
    url(r'^explore/', include('explore.urls')),
    url(r'^visualize/', include('visualize.urls')),
#     (r'^planner/', include('visualize.urls')),
#     (r'^embed/', include('visualize.urls')),
#     (r'^mobile/', include('visualize.urls')),
#     (r'^feedback/', include('feedback.urls')),
#     (r'^proxy/', include('proxy.urls')),
#     (r'^$', redirect_to, {'url': '/portal/'}),
#     (r'', include('madrona.common.urls')),

# from madrona.common.urls: 
# if settings.LAUNCH_PAGE:
#     urlpatterns = patterns('madrona.common.views',
#         url(r'^$', 'launch', name='launch'),
#         url(r'^map/', 'map', name='map'),
#     )
# else:
#     urlpatterns = patterns('madrona.common.views',
#         url(r'^$', 'map', name='map'),
#     )
# 
# urlpatterns += patterns('madrona',
#     (r'^accounts/', include('madrona.openid.urls')),
#     (r'^accounts/profile/', include('madrona.user_profile.urls')),
#     (r'^faq/', include('madrona.simplefaq.urls')),
    url(r'^features/', include('features.urls')),
#     (r'^help/', include('madrona.help.urls')),
#     (r'^kml/', include('madrona.kmlapp.urls')),
#     (r'^layers/', include('madrona.layers.urls')),
#     (r'^loadshp/', include('madrona.loadshp.urls')),
#     (r'^manipulators/', include('madrona.manipulators.urls')),
#     (r'^news/', include('madrona.news.urls')),
#     (r'^screencasts/', include('madrona.screencasts.urls')),
#     (r'^staticmap/', include('madrona.staticmap.urls')),
#     (r'^studyregion/', include('madrona.studyregion.urls')),
#     (r'^bookmark/', include('madrona.bookmarks.urls')),
#     (r'^layer_manager/', include('madrona.layer_manager.urls')),
#     # Optional
#     #(r'^heatmap/', include('madrona.heatmap.urls')),
# )
# 
# urlpatterns += patterns('',
#     (r'^admin/', include(admin.site.urls)),
# )
# 
# # Useful for serving files when using the django dev server
# urlpatterns += patterns('',
#     (r'^media(.*)/upload/', 'madrona.common.views.forbidden'),
#     (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
# )



)
