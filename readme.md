# Marine Planner Refactor


## Development environment

- Get homebrew
- `brew install python gdal geos`
- `pip install virtualenv`
- `python -m virtualenv ~/path/to/mpr`
- `. ~/path/to/mpr/bin/activate`
- `git clone marco-redesign`
- `cd marco-redesign`
- `sh install.sh` # fetch the other marco modules

Note: install.sh is a hack. It `git clone`'s all of the libraries that are dependent, and then
`pip install -e`'s them. This seems to work right now but is not maintainable. 

Requirements: 

- django==1.7

- If using Postgres.app, add it's bin folder to the path. Put the following in your `~/.bash_profile`
`PATH=/Applications/Postgres.app/Contents/MacOS/bin:$PATH`


## Production Deployment

TBD

## Development Deployment

### Dependencies

This project consists of several modules that are factored out of Madrona 4.1 and Marine Planner from the Marco branch. 

Installation procedure: 

- Create a project directory, `~/projects/marco`
- Clone all of the repositories to `~/project/mpr`, so you'll have `~/projects/mpr/marco`, `~/project/mpr/madrona-features`, etc. 
- Make sure your virtual environment is active, and run `pip install -e .` inside each directory (except the main one). 
- in the main marco directory, `python manage.py migrate`, then `python manage.py runserver`


### Development Notes

After installing the database, you have to add the cleangeometry() function from cleangeometry.sql. 

With PostGIS 2.0, cleangeometry.sql has to be modified to use ST_IsValid() instead of IsValid(). (TODO)

Add the custom albers projection into `spatial_ref_sys`
```
insert into spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) 
values (99996, 'EPSG', 99996, 'Marco Albers', '+proj=aea +lat_1=37.25 +lat_2=40.25 +lat_0=36 +lon_0=-72 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs')
```

Add the albers projection into the bottom of the `epsg` file. 

```
# Marco Albers
<99996> +proj=aea +lat_1=37.25 +lat_2=40.25 +lat_0=36 +lon_0=-72 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs <>
```

This file lives in various places; the one you want is the one that is loaded by the GDAL that python is using. If you've installed GDAL via homebrew, then it's something like `/usr/local/Cellar/proj/4.8.0/share/proj/epsg`.

On linux, the file will be in `/usr/local/share/proj/epsg` or `/usr/share/proj/epsg`.


