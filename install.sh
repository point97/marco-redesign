if [ -z $VIRTUAL_ENV ]; then
    echo "You're not in a virtual environment, activate one and then try again."
    exit 1
fi

git clone git@bitbucket.org:point97/madrona-analysistools.git
git clone git@bitbucket.org:point97/madrona-forms.git
git clone git@bitbucket.org:point97/madrona-scenarios.git
git clone git@bitbucket.org:point97/madrona-manipulators.git
git clone git@bitbucket.org:point97/madrona-openid.git
git clone git@bitbucket.org:point97/mp-clipping.git
git clone git@bitbucket.org:point97/mp-drawing.git
git clone git@bitbucket.org:point97/mp-explore.git
git clone git@bitbucket.org:point97/mp-profile.git
git clone git@bitbucket.org:point97/mp-visualize.git
git clone git@bitbucket.org:point97/mp-data-manager.git
git clone git@bitbucket.org:point97/p97-nursery.git

pip install -e madrona-analysistools
pip install -e madrona-forms
pip install -e madrona-scenarios
pip install -e madrona-manipulators
pip install -e madrona-openid
pip install -e mp-clipping
pip install -e mp-drawing
pip install -e mp-explore
pip install -e mp-profile
pip install -e mp-visualize
pip install -e mp-data-manager
pip install -e p97-nursery
